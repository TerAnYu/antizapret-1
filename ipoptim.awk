BEGIN {
cone = 0

ctwo03 = 0
ctwo46 = 0
ctwo79 = 0

cthree03 = 0
cthree46 = 0
cthree79 = 0
}

# Only one digit in the first octet
/^[0-9]\./ {one[cone] = $0; cone++}

# Two digits
/^[0-9][0-3]\./ {two03[ctwo03] = $0; ctwo03++}
/^[0-9][4-6]\./ {two46[ctwo46] = $0; ctwo46++}
/^[0-9][7-9]\./ {two79[ctwo79] = $0; ctwo79++}

# Three digits
/^[0-9][0-9][0-3]\./ {three03[cthree03] = $0; cthree03++}
/^[0-9][0-9][4-6]\./ {three46[cthree46] = $0; cthree46++}
/^[0-9][0-9][7-9]\./ {three79[cthree79] = $0; cthree79++}

# Final function
END {
 print "one = ["
 for (i=0; i<cone; i++)
  print "\"" one[i] "\","
 print "];"

 print ""

 print "two03 = ["
 for (i=0; i<ctwo03; i++)
  print "\"" two03[i] "\","
 print "];"

 print ""

 print "two46 = ["
 for (i=0; i<ctwo46; i++)
  print "\"" two46[i] "\","
 print "];"

 print ""

 print "two79 = ["
 for (i=0; i<ctwo79; i++)
  print "\"" two79[i] "\","
 print "];"

 print ""

 print "three03 = ["
 for (i=0; i<cthree03; i++)
  print "\"" three03[i] "\","
 print "];"

 print ""

 print "three46 = ["
 for (i=0; i<cthree46; i++)
  print "\"" three46[i] "\","
 print "];"

 print ""

 print "three79 = ["
 for (i=0; i<cthree79; i++)
  print "\"" three79[i] "\","
 print "];"

 print ""


}
