#!/bin/sh

# DNS server IP address to generate dnsmasq config
DNSPROXYIP='195.123.209.38'

# HTTPS (TLS) proxy address
PACHTTPSHOST='proxy.antizapret.prostovpn.org:3143'

# Usual proxy address
PACPROXYHOST='proxy.antizapret.prostovpn.org:3128'

#Primary DNS which is used to resolve address
DNS1='74.82.42.42'
#DNS1='8.8.4.4'

# Secondary DNS for resolving
#DNS2='8.8.8.8'

# How many flows of jobs
JOBSFLOWNUM=3

# How many hostname resolved in 1 jobs
HOSTRESJB=128

# dnsmasq-aliases.conf path file
DAPF='/etc/dnsmasq.d/'

# Path OpenVPN .ccd config
OVPNCCDPH='/etc/openvpn/ccd/DEFAULT'

# PAC file storage location
PFSL='/usr/share/nginx/html/antizapret/'

# setpacuser.sh logfile path
SLP='/var/log/nginx'
